﻿using EZEffects;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControllerInput : MonoBehaviour {

    public AudioClip clip;
    private AudioSource audioSource;

    private Transform activeGun;
    public Transform rightGun;
    public Transform leftGun;
    public EffectMuzzleFlash MuzzleEffect;
    public EffectImpact WoodImpactEffect;
    public EffectImpact StoneImpactEffect;
    public EffectImpact MetalImpactEffect;

    void Start () {
        audioSource = GetComponent<AudioSource>();
        audioSource.clip = clip;
	}




    void Update () {
 
        if (OVRInput.GetDown(OVRInput.Button.SecondaryIndexTrigger))
        {
            activeGun = rightGun;
            FireGun();
        }

        if (OVRInput.GetDown(OVRInput.Button.PrimaryIndexTrigger))
        {
            activeGun = leftGun;
            FireGun();
        }
    }

    private void FireGun()
    {
        MuzzleEffect.ShowMuzzleEffect(activeGun.transform, true, audioSource);
        // audioSource.Play();
        RaycastGun();
    }

    private void RaycastGun()
    {
        RaycastHit hit;

        if (Physics.Raycast(activeGun.position, activeGun.forward, out hit))
        {
            // Debug.Log("Hit at " + hit.distance);
            

            if (hit.collider.gameObject.CompareTag("Wood"))
            {
                WoodImpactEffect.ShowImpactEffect(hit.point, hit.normal);
            }

            if (hit.collider.gameObject.CompareTag("Stone"))
            {
                StoneImpactEffect.ShowImpactEffect(hit.point, hit.normal);
            }

            if (hit.collider.gameObject.CompareTag("Metal"))
            {
                MetalImpactEffect.ShowImpactEffect(hit.point, hit.normal);
            }
        }
    }
}
